var class_c_bug_demo_view =
[
    [ "CBugDemoView", "class_c_bug_demo_view.html#aacf9674d7d5a98a033d008feff6aed7b", null ],
    [ "~CBugDemoView", "class_c_bug_demo_view.html#abb9541422f8a7d14d2fe1d6e41011bf6", null ],
    [ "Create", "class_c_bug_demo_view.html#ae6ae226cc8cbfdfa51efea660123e268", null ],
    [ "EmptyBasket", "class_c_bug_demo_view.html#a63beb6af5213c116b19a3c068fea7f8d", null ],
    [ "GetBug", "class_c_bug_demo_view.html#a114456681030dc87eaf06c6ae271a0bd", null ],
    [ "Harvest", "class_c_bug_demo_view.html#a780576f4e6692be1e45c1976a0204929", null ],
    [ "OnEraseBkgnd", "class_c_bug_demo_view.html#acb0b12ef398e5ebb4ea52f62efee99ec", null ],
    [ "OnPaint", "class_c_bug_demo_view.html#a10339367010a39c4a3a0844a71fa34d3", null ],
    [ "RegisterWindowClass", "class_c_bug_demo_view.html#a6372bedecdda82af5e7a4d748b3b50a3", null ],
    [ "SetBendAngle", "class_c_bug_demo_view.html#a62b36ee42cb4b04fe835e098de8cd214", null ],
    [ "SetFrame", "class_c_bug_demo_view.html#a2e7d2758f303ac86d78b0a83c22de44a", null ],
    [ "SetSeed", "class_c_bug_demo_view.html#aecedd131a92cfcc769c74928eb5d5527", null ],
    [ "mBasket", "class_c_bug_demo_view.html#a7a6f09d4363f983f6d555b3a3a8bb153", null ],
    [ "mBug", "class_c_bug_demo_view.html#a735ae08c100bc663b01e2ee4973fca75", null ]
];