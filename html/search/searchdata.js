var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwx~",
  1: "acdeik",
  2: "abcdhilmprstvx",
  3: "abcdefghiklmoprstuvwx~",
  4: "beghlmprstw",
  5: "t",
  6: "nu",
  7: "_"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros"
};

