/**
 * \file Picture.cpp
 *
 * \author Charles B. Owen
 */

#include "stdafx.h"
#include "Picture.h"
#include "Actor.h"
#include "BugAdapter.h"
#include "BasketAdapter.h"
#include "Head.h"
using namespace Gdiplus;
using namespace xmlnode;

/**
 * \brief Constructor
 */
CPicture::CPicture()
{
	
}


/**
 * \brief Destructor
 */
CPicture::~CPicture()
{
}


/**
 * Removes an actor from the picture's container of actors.
 * \param actor Pointer to the actor to be removed.
 */
void CPicture::RemoveActor(CActor * actor)
{
	for (std::vector< std::shared_ptr<CActor> >::iterator child = mActors.begin(); child != mActors.end(); ++child )
	{
		if (child->get() == actor)
		{
			mActors.erase(child);
			break;
		}
	}
}

/** \brief Set the current animation time
*
* This forces the animation of all
* objects to the current animation location.
* \param time The new time.
*/
void CPicture::SetAnimationTime(double time)
{
    mTimeline.SetCurrentTime(time);
	for (std::shared_ptr<CBugAdapter> bug : mBugs)
	{
		bug->Update(mTimeline.GetCurrentFrame());
	}

    UpdateObservers();

    for (auto actor : mActors)
    {
        actor->GetKeyframe();
    }
}

/** \brief Add an observer to this picture.
* \param observer The observer to add
*/
void CPicture::AddObserver(CPictureObserver *observer)
{
    mObservers.push_back(observer);
}

/** \brief Remove an observer from this picture
* \param observer The observer to remove
*/
void CPicture::RemoveObserver(CPictureObserver *observer)
{
    auto loc = find(std::begin(mObservers), std::end(mObservers), observer);
    if (loc != std::end(mObservers))
    {
        mObservers.erase(loc);
    }
}

/** \brief Update all observers to indicate the picture has changed.
*/
void CPicture::UpdateObservers()
{
    for (auto observer : mObservers)
    {
        observer->UpdateObserver();
    }
}

/** \brief Draw this picture on a device context
* \param graphics The device context to draw on
*/
void CPicture::Draw(Gdiplus::Graphics *graphics)
{
    for (auto actor : mActors)
    {
        actor->Draw(graphics);
    }
}


/** Add an actor to this picture.
* \param actor The actor to add
*/
void CPicture::AddActor(std::shared_ptr<CActor> actor)
{
    mActors.push_back(actor);
    actor->SetPicture(this);
}

/** \brief Get the current animation time.
* \returns The current animation time
*/
double CPicture::GetAnimationTime()
{
    return mTimeline.GetCurrentTime();
}


/**
 * Generate the actor of a bug.
 * \param pos The position to place the actor at. 
 * \returns Shared pointer to the constructed Actor.
 */
std::shared_ptr<CActor> CPicture::GetBugActor(Gdiplus::Point pos)
{
	std::shared_ptr<CActor> bug = std::make_shared<CActor>(L"Bug");
	bug->SetClickable(false);
	bug->SetPosition(pos);
	return bug;
}


/**
* \brief Save the picture animation to a file
* \param filename File to save to.
*/
void CPicture::Save(const std::wstring &filename)
{
    //
    // Create an XML document
    //
    auto root = CXmlNode::CreateDocument(L"anim");

    // Save the timeline animation into the XML
    mTimeline.Save(root);

    //
    // It is possible to add attributes to the root node here
    //
    //root->SetAttribute(L"something", mSomething);
	int index = 0;
	for (std::shared_ptr<CBugAdapter> bug : mBugs)
	{
		bug->Save(root, index);
		index++;
	}

    try
    {
        root->Save(filename);
    }
    catch (CXmlNode::Exception ex)
    {
        AfxMessageBox(ex.Message().c_str());
    }
}



/**
* \brief Load a picture animation from a file
* \param filename file to load from
*/
void CPicture::Load(const std::wstring &filename)
{
    // We surround with a try/catch to handle errors
    try
    {
        // Open the document to read
        auto root = CXmlNode::OpenDocument(filename);
        
        // Load the animation from the XML
        mTimeline.Load(root);

        //
        // It is possible to load attributes from the root node here
        //
        // mSomething = root->GetAttributeIntValue(L"something", 123);
		for (std::shared_ptr<CBugAdapter> bug : mBugs)
		{
			bug->Remove();
		}
		for (std::shared_ptr<xmlnode::CXmlNode> node : root->GetChildren())
		{
			if (node->GetName() == L"bug")
			{
				int seed = node->GetAttributeIntValue(L"Seed", 0);
				int x = node->GetAttributeIntValue(L"PosX", 0);
				int y = node->GetAttributeIntValue(L"PosY", 0);
				std::shared_ptr<CBugAdapter>  bug = InsertLoadedBug(seed, Gdiplus::Point(x, y));
				bug->Load(node);
			}
		}

        SetAnimationTime(0);
        UpdateObservers();

    }
    catch (CXmlNode::Exception ex)
    {
        AfxMessageBox(ex.Message().c_str());
    }

}

/**
 * Insert a new bug into the picture
 * \param pos The position the bug's root segment will be in the picture.
 */
void CPicture::InsertBug(Gdiplus::Point pos)
{
	std::shared_ptr<CActor> bug = GetBugActor(pos);
	std::shared_ptr<CBugAdapter> bug1Image = std::make_shared<CBugAdapter>(L"Bug 1 Adapter");
	bug->AddDrawable(bug1Image);
	bug->SetRoot(bug1Image);
	mBugs.push_back(bug1Image);
	AddActor(bug);

	bug1Image->SetStartFrame(mTimeline.GetCurrentFrame());
}

/**
 * Harvest all the bugs with heads in the picture.
 */
void CPicture::Harvest()
{
	for (std::shared_ptr<CBugAdapter> bug : mBugs)
	{
		std::shared_ptr<CHead> head = bug->Harvest();
		if (head != nullptr)
		{
			bug->SetHarvestFrame(mTimeline.GetCurrentFrame());
			mBasket->AddToBasket(head);
		}
		UpdateObservers();
	}

}


/**
 * Insert a bug into the picture
 * \param seed Seed of the bug's random generator
 * \param pos Position in the picture to place bug's root segment
 * \returns The pointer of the new bug.
 */
std::shared_ptr<CBugAdapter>  CPicture::InsertLoadedBug(int seed, Gdiplus::Point pos)
{
	
	std::shared_ptr<CActor> bug = GetBugActor(pos);
	std::shared_ptr<CBugAdapter> bug1Image = std::make_shared<CBugAdapter>(L"Bug Adapter", seed);
	bug->AddDrawable(bug1Image);
	bug->SetRoot(bug1Image);
	mBugs.push_back(bug1Image);
	AddActor(bug);

	bug1Image->SetStartFrame(mTimeline.GetCurrentFrame());
	
	return bug1Image;
}
