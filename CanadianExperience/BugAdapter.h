/**
 * \file BugAdapter.h
 *
 * \author Samantha Oldenburg
 *
 * Describes a class that adapts the CBug class in the BugLib to
 * be used as a drawable.
 */

#pragma once
#include "Drawable.h"
#include "Bug.h"


/**
 * Adapter for the CBug class in the BugLib to
 * be used as a drawable.
 */
class CBugAdapter :
	public CDrawable
{
public:
	CBugAdapter(const std::wstring &name);
	CBugAdapter(const std::wstring &name, int seed);

	

	virtual ~CBugAdapter();
	
	void SetStartFrame(int frame);

	virtual bool HitTest(Gdiplus::Point point) override;
	virtual void Draw(Gdiplus::Graphics * graphics) override;

	std::shared_ptr<xmlnode::CXmlNode> Save(const std::shared_ptr<xmlnode::CXmlNode> &root, int index);

	void Load(const std::shared_ptr<xmlnode::CXmlNode> &node);

	void Remove();

	void Update(int frame);

	/** Initiates a harvest for the bug 
	 * \returns a pointer to a head or nullptr if none is found.*/
	std::shared_ptr<CHead> Harvest() { return mBug->Harvest(); }

	void SetSeed(int seed);

	/** \brief Get the DRAWABLE'S ACTOR'S position, as the location of 
	 * the bug is held in the actor.
	* \returns The drawable's actor's position*/
	//virtual const Gdiplus::Point GetPosition() override;

	/** Set the frame at which a harvest occurs. 
	 * \param harvest The frame which the harvest occurred. */
	void SetHarvestFrame(int harvest) { mHarvestFrame = harvest; }
private:

	/// CBug object to be adapted.
	std::shared_ptr<CBug> mBug;


	/// Frame to start growing bug.
	int mStartFrame = 3000000;

	/// Frame to initiate harvest at.
	int mHarvestFrame = 0;


};

