/**
 * \file BasketAdapter.h
 *
 * \author Samantha Oldenburg
 *
 * Class that adapts the Basket class from BugLib into a Drawable. 
 */

#pragma once
#include "Drawable.h"
class CBasket;
class CHead;

/**
 * Class that adapts the Basket class from BugLib into a Drawable. 
 */
class CBasketAdapter :
	public CDrawable
{
public:
	CBasketAdapter(const std::wstring &name);
	virtual ~CBasketAdapter();


	void AddToBasket(std::shared_ptr<CHead> head);
	virtual bool HitTest(Gdiplus::Point point) override;
	virtual void Draw(Gdiplus::Graphics * graphics);
private:
	/// Pointer to the basket object this will adapt.
	std::shared_ptr<CBasket> mBasket;
};

