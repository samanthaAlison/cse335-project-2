/**
 * \file InsertBugDlg.cpp
 *
 * \author Samantha Oldenburg
 */

// InsertBugDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CanadianExperience.h"
#include "InsertBugDlg.h"
#include "afxdialogex.h"


// CInsertBugDlg dialog

IMPLEMENT_DYNAMIC(CInsertBugDlg, CDialog)

/**
 * Constructor.
 * \param pParent Window that is the parent of this dialog.
 */
CInsertBugDlg::CInsertBugDlg(CWnd* pParent /*=NULL*/)
	: CDialog(IDD_INS_BUG_DLG, pParent)
	, mXPos(0)
	, mYPos(0)
{

}

/**
 * Destructor. 
 */
CInsertBugDlg::~CInsertBugDlg()
{
}

/**
 * Do the data exchange.
 * \param pDX Data to exchange.
 */
void CInsertBugDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_XPOS_EDIT, mXPos);
	DDX_Text(pDX, IDC_YPOS_EDIT, mYPos);
}


BEGIN_MESSAGE_MAP(CInsertBugDlg, CDialog)
END_MESSAGE_MAP()


// CInsertBugDlg message handlers
