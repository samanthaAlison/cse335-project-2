/**
 * \file BasketAdapter.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "BasketAdapter.h"
#include "Basket.h"
#include "BugFactory.h"


/**
 * Constructor. 
 * \param name Name of drawable.
 */
CBasketAdapter::CBasketAdapter(const std::wstring &name) : CDrawable(name)
{
	CBugFactory factory;
	mBasket = factory.CreateBasket();
}


/**
 * Destructor.
 */
CBasketAdapter::~CBasketAdapter()
{
}


/**
 * Adds a head to the adapter's basket.
 * \param head The head we are adding to the basket.
 */
void CBasketAdapter::AddToBasket(std::shared_ptr<CHead> head)
{
	mBasket->AddToBasket(head);
}


/**
 * Special hit test for basket adapter
 * \param point Point we are testing
 * \returns False, always
 */
bool CBasketAdapter::HitTest(Gdiplus::Point point)
{
	return false;
}


/**
 * Draw the basket adapter
 * \param graphics Pointer to the graphics object we are using.
 */
void CBasketAdapter::Draw(Gdiplus::Graphics * graphics)
{
	Gdiplus::GraphicsState state = graphics->Save();
	graphics->TranslateTransform((float)mPlacedPosition.X, (float)mPlacedPosition.Y);
	graphics->RotateTransform((float)mPlacedR);
	graphics->ScaleTransform(0.3f, 0.3f);
	mBasket->DrawBasket(graphics);
	graphics->Restore(state);
}
