/**
 * \file BugAdapter.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "BugAdapter.h"
#include "BugFactory.h"
#include "BugSeedDlg.h"
#include "Actor.h"
#include "Picture.h"
 /// Scale to draw bug at.
const float BugScale(0.3f);


/**
 * Constructor. Calls the BugSeedDlg
 * \param name Name of the bug.
 */
CBugAdapter::CBugAdapter(const std::wstring &name) : CDrawable(name)
{
	CBugFactory factory;
	mBug = factory.CreateBug();
	CBugSeedDlg dlg(mBug);
	if (dlg.DoModal() == IDOK) {
		// A new seed has been set
	}
	mBug->SetBend(31.4 / 8.0);
	
}

/**
 * Constructor. Doesn't call the BugSeedDlg.
 * \param name The name of the bug.
 * \param seed The seed of the bug.
 */
CBugAdapter::CBugAdapter(const std::wstring & name, int seed) : CDrawable(name)
{
	CBugFactory factory;
	mBug = factory.CreateBug();
	SetSeed(seed);

	mBug->SetBend(31.4 / 8.0);
}


/**
 * Destructor.
 */
CBugAdapter::~CBugAdapter()
{
	
}


/**
 * Sets the frame at which the bug will start growing.
 * \param frame The frame at which the bug will start growimg.
 */
void CBugAdapter::SetStartFrame(int frame)
{
	mStartFrame = frame;
}

/** \brief Test to see if we have been clicked on by the mouse
* \param point Position to test
* \returns true if clicked on */
bool CBugAdapter::HitTest(Gdiplus::Point point)
{
	return false;
}

/**
 * Draws the bug.
 * \param graphics Pointer to the graphics object.
 */
void CBugAdapter::Draw(Gdiplus::Graphics * graphics)
{
	
	Gdiplus::GraphicsState state = graphics->Save();
	graphics->TranslateTransform((float)mPlacedPosition.X, (float)mPlacedPosition.Y);
	graphics->RotateTransform((float)mPlacedR);
	graphics->ScaleTransform(BugScale, BugScale);
	mBug->DrawBug(graphics);
	graphics->Restore(state);
}

/**
 * Saves the bug into the XML file as a new XML node;
 * \param root The root node of the xml file.
 * \param index The index of the bug in the picture's container of bugs.
 * \returns The new XML node that was created.
 */
std::shared_ptr<xmlnode::CXmlNode> CBugAdapter::Save(const std::shared_ptr<xmlnode::CXmlNode> &root, int index)
{
	std::shared_ptr<xmlnode::CXmlNode> bugNode = root->AddChild(L"bug");
	bugNode->SetAttribute(L"ID", index);
	bugNode->SetAttribute(L"Name", GetName());


	int x = GetActor()->GetPosition().X;
	int y = GetActor()->GetPosition().Y;
	bugNode->SetAttribute(L"PosX", x);
	bugNode->SetAttribute(L"PosY", y);
	
	bugNode->SetAttribute(L"Seed", mBug->GetSeed());
	bugNode->SetAttribute(L"StartFrame", mStartFrame);
	bugNode->SetAttribute(L"HarvestFrame", mHarvestFrame);

	return bugNode;
}

/**
 * Loads the bug from an XML node
 * \param node The XML node that contains the load information.
 */
void CBugAdapter::Load(const std::shared_ptr<xmlnode::CXmlNode> &node)
{

	mStartFrame = node->GetAttributeIntValue(L"StartFrame", 0);
	mHarvestFrame = node->GetAttributeIntValue(L"HarvestFrame", 0);
}

/**
 * Removes the bug from the picture by
 * removing its actor. */
void CBugAdapter::Remove()
{
	if (GetActor() != nullptr)
		GetActor()->GetPicture()->RemoveActor(GetActor());
}


/**
 * Updates the bug when the frame of the timeline changes.
 * \param frame The current frame of the timeline.
 */
void CBugAdapter::Update(int frame)
{
	if (frame == 0)
	{
		SetSeed(mBug->GetSeed());
		mBug->SetBugFrame(0);
	}
	int bugFrame = frame - mStartFrame;
	if (bugFrame >= 0)
	{
		mBug->SetBugFrame(bugFrame);
		if (frame - mHarvestFrame < 5 && frame - mHarvestFrame > -5)
		{
			Harvest();
		}
	}

}


/**
 * Sets the seed of the CBug object in this adapter.
 * \param seed The new seed of the bug.
 */
void CBugAdapter::SetSeed(int seed)
{
	mBug->SetSeed(seed);
}

