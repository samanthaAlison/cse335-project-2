/**
 * \file InsertBugDlg.h
 *
 * \author Samantha Oldenburg
 *
 * Dialog that inserts a bug into the picture
 */

#pragma once


// CInsertBugDlg dialog

/**
 * Dialog that inserts a bug into the picture
 */
class CInsertBugDlg : public CDialog
{
	DECLARE_DYNAMIC(CInsertBugDlg)

public:
	CInsertBugDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CInsertBugDlg();

	/// ID of the dialog
	enum { IDD = IDD_INS_BUG_DLG };

	/** Get the position from the dialog. Used to determine the
	 * position of a created bug.
	 * \returns A point containing the position. */
	Gdiplus::Point GetPosition() { return Gdiplus::Point(mXPos, mYPos); }
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
	/// X-Coordinate of the bug to be inserted
	int mXPos;
	
	/// Y-Coordinate of the bug to be inserted
	int mYPos;
};
