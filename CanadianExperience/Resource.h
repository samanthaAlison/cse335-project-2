//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CanadianExperience.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_CanadianExperieTYPE         130
#define IDR_VIEWEDIT                    131
#define IDR_VIEWACTORS                  132
#define IDR_VIEWTIMELINE                133
#define IDB_BITMAP1                     311
#define IDB_HAROLD                      311
#define IDD_TIMELINEDLG                 312
#define IDD_INS_BUG_DLG                 314
#define IDC_NUMFRAMES                   1000
#define IDC_EDIT2                       1001
#define IDC_FRAMERATE                   1001
#define IDC_EDIT3                       1003
#define IDC_YPOS_EDIT                   1003
#define IDC_XPOS_EDIT                   1004
#define ID_FILE_TEST                    32771
#define ID_EDIT_MOVE                    32772
#define ID_EDIT_ROTATE                  32773
#define ID_EDIT_SETKEYFRAME             32774
#define ID_EDIT_DELETEKEYFRAME          32775
#define ID_BUTTON32779                  32779
#define ID_EDIT_TIMELINEPROPERTIES      32780
#define ID_FILE_SAVEAS                  32781
#define ID_FILE_OPEN32782               32782
#define ID_PLAY_PLAY                    32783
#define ID_PLAY_PLAYFROMBEGINNING       32784
#define ID_PLAY_STOP                    32785
#define ID_BUGS_INSERTBUG               32789
#define ID_BUGS_HARVEST                 32790
#define ID_BUGS_STARTGROW               32791
#define ID_PLAY_PLAY32792               32792
#define ID_PLAY_PLAYFROMSTART           32793
#define ID_PLAY_STOP32794               32794

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        316
#define _APS_NEXT_COMMAND_VALUE         32795
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
