/**
 * \file Hardpoint.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "Hardpoint.h"
#include "BugImage.h"

/**
 * Constructor.
 * \param parent The parent of this hardpoint. 
 */
CHardpoint::CHardpoint(CBugImage * parent) : mParent(parent)
{
	
}

/**
 * Destructor.
 */
CHardpoint::~CHardpoint()
{

}

/**
 * Call to add a child. This adds the child to the mToAdd
 * container. The child will be actually added at the next call 
 * of FinalizeAdd().
 * \param child Child to be added. */
void CHardpoint::AddChild(std::shared_ptr<CBugImage> child)
{
	mToAdd.push_back(child);
	child->SetParentPoint(this);
}


/**
 * Sets the position of this hardpoint. All of this
 * hardpoint's children will move to here in the next
 * update cycle.
 * \param point The point to move the hardpoint to.
 */
void CHardpoint::SetPosition(Gdiplus::PointF point)
{
	mPosition = point;
}

/**
 * Gets the position of the hardpoint relative to its parent's scale.
 * means that if the parent's scale is 0.5, the hardpoint will be 2
 * times closer to the parents center point than if the scale was 1.0.
 * necessary to ensure that body parts of the bug are connected as it 
 * grows.
 * \returns The position of the hardpoint adjusted for scale. 
 */
Gdiplus::PointF CHardpoint::GetPosition()
{
	Gdiplus::PointF distance = mPosition - mParent->GetCenterPoint();
		
	double scale = mParent->GetScale();
	return mParent->GetCenterPoint() + Gdiplus::PointF(
		distance.X * (float)scale,
		distance.Y * (float)scale
	);
}

/**
 * Set the parent BugImage of this Hardpoint.
 * \param image Pointer to the BugImage that is new the parent.
 */
void CHardpoint::SetParent(CBugImage * image)
{
	mParent = image;
}


/**
 * Rotates the hardpoint with its own point as the axis of rotation.
 * Causes the hardpoint's children to rotate around that point as well.
 * \param rotate Angle to rotate hardpoint in radians.
 */
void CHardpoint::Rotate(double rotate)
{
	for (std::shared_ptr<CBugImage> child : mChildren)
	{
		child->Rotate(rotate);
	}
}

/**
 *  Gets the angle of a line formed between two points, assuming
 *	one of the points to be at coordinates (0, 0)
 * \param pointA The point to be the origin
 * \param pointB The point we are finding the angle of in r
 * relation to pointA
 * \returns The angle between the two points in radians.
 */
double CHardpoint::GetAngleOfPoints(Gdiplus::PointF pointA, Gdiplus::PointF pointB)
{
	Gdiplus::PointF line = pointB - pointA;
	return atan2(line.Y, line.X);
}

/**
 * Rotates the hardpoint around a specific point, such that the distance 
 * between the hardpoint and the point remains constant, but the angle 
 * between the changes, and the hardpoint orbits the point.
 * \param theta Amount to rotate the hardpoint in radians.
 * \param point The point the hardpoint should orbit around.
 */
void CHardpoint::RotateAround(double theta, Gdiplus::PointF point)
{

	
	// Get the angle that this hardpoint is currently at with the point.
	double currentTheta = GetAngleOfPoints(point, mPosition);

	// Get the angle that the hardpoint will be at after the transform.
	double totalTheta = theta + currentTheta;

	// Get the distance between the hardpoint and the point we are rotating
	// around.

	Gdiplus::PointF dPosition = point - mPosition;
	double radius = sqrt(
		(dPosition.X * dPosition.X) +
		(dPosition.Y * dPosition.Y)
	);

	double cosT = cos(totalTheta);
	double sinT = sin(totalTheta);

	// Set the new x, y coordinates of the hardpoint due to rotation.
	mPosition = Gdiplus::PointF(
		point.X + (float)radius * (float)cos(totalTheta),
		point.Y + (float)radius * (float)sin(totalTheta)
	);
	Gdiplus::PointF nDPosition = point - mPosition;
	double newRadius = sqrt(
		(nDPosition.X * nDPosition.X) +
		(nDPosition.Y * nDPosition.Y)
	);

	if (abs(radius - newRadius) > 0.001)
	{
		printf("%f - %f = %f", radius, newRadius, radius - newRadius);
	}

	// Apply rotation to children
	for (std::shared_ptr<CBugImage> child : mChildren)
	{
		child->Rotate(theta);
	}
}

/**
 * Removes objects in the mToRemove container from the hardpoint's children.
 */
void CHardpoint::FinalizeRemove()
{
	std::vector<std::shared_ptr<CBugImage> >::iterator child = mToRemove.begin();
	if (child != mToRemove.end())
	{
		std::vector<std::shared_ptr<CBugImage> >::iterator iter =
			std::find(mChildren.begin(), mChildren.end(), *child);
		if (iter != mChildren.end())
		{
			mChildren.erase(iter);
			mToRemove.erase(child);
			this->FinalizeRemove();
		}
	}
}



/**
 * Adds objects in the mToAdd container to the hardpoint's children.
 */
void CHardpoint::FinalizeAdd()
{
	std::vector<std::shared_ptr<CBugImage> >::iterator child = mToAdd.begin();
	if (child != mToAdd.end())
	{
		mChildren.push_back(*child);
		mToAdd.erase(child);
		FinalizeAdd();
	}

}



/**
 * Updates the hardpoint, first by adding and removing 
 * children from the appropriate containers. Then, updating 
 * the hardpoint's children in the updated children container.
 */
void CHardpoint::Update()
{
	
	if ((mToAdd.size() + mChildren.size() - mToRemove.size() ) == 0 )
	{
		Spawn();
	}
	FinalizeRemove();
	
	FinalizeAdd();
	for (std::shared_ptr<CBugImage> child : mChildren)
	{
		child->Update();
	}
}


/**
 * Called to remove a child. This adds the child to the mToRemove 
 * container. The child will be actually removed at the next update
 * call.
 * \param image Child to be removed.
 */
void CHardpoint::RemoveChild(CBugImage * image)
{	
	for (std::vector<std::shared_ptr<CBugImage> >::iterator child = mChildren.begin();
	 child != mChildren.end();
		++child)
	{
		if (child->get() == image)
		{
			mToRemove.push_back(*child);
			break;


		}
	}
}



/**
 * Draws the hardpoint's children. This function is called by
 * this hardpoint's parent.
 * \param graphics Pointer to the graphics object.
 */
void CHardpoint::Draw(Gdiplus::Graphics * graphics)
{

	
	for (std::shared_ptr<CBugImage> child : mChildren)
	{
		child->DrawImage(graphics);
		
	}

}