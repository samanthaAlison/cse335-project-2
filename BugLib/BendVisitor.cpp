#include "stdafx.h"
#include "BendVisitor.h"
#include "Segment.h"
#include "HeadActual.h"
#include "Leg.h"
#include "Stinger.h"

/**
 * Constructor. 
 * \param bend Angle to bend segments at.
 */
CBendVisitor::CBendVisitor(double bend) : mBend(bend)
{
}


/**
 * Destructor.
 */
CBendVisitor::~CBendVisitor()
{

	
}

/**
 * Visit a body part of a bug.
 * \param bodyPart Pointer to the body part we are visiting.
 */
void CBendVisitor::VisitBodyPart(CBodyPart * bodyPart)
{
}

/**
* Visit a leg of a bug.
* \param leg Pointer to the leg we are visiting.
*/
void CBendVisitor::VisitLeg(CLeg * leg)
{
}

/**
* Visit a segment of a bug. Bends the segment.
* \param segment Pointer to the segment we are visiting.
*/
void CBendVisitor::VisitSegment(CSegment * segment)
{
	
	segment->Rotate(mBend);
	
}


/**
 * Visit a head of a bug. Bends the head.
 * \param head Pointer to the head of the bug we are visiting.
 */
void CBendVisitor::VisitHead(CHeadActual * head)
{
	head->Rotate(mBend);
}


/**
 * Visit a stinger of a bug. Bends the stinger.
 * \param stinger Pointer to the stinger we are visiting.
 */
void CBendVisitor::VisitStinger(CStinger * stinger)
{
	stinger->Rotate(mBend);
}
