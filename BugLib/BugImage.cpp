/**
 * \file BugImage.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include <iterator>
#include <algorithm>
#include "BugImage.h"
#include "Hardpoint.h"
/// Constant ratio to convert radians to degrees
const double RtoD = 57.295779513;


/**
 * Copy constructor. 
 * \param bugImage The bugimage we are copying
 */
CBugImage::CBugImage(const CBugImage &bugImage)
{
	mMirror = bugImage.mMirror;
	SetImage(bugImage.mFilename.c_str());
	mScale = bugImage.mScale;
	Rotate(bugImage.mRotation);
	mParentPoint = bugImage.mParentPoint;
	
	mCenterPoint = bugImage.mCenterPoint;

	std::for_each(bugImage.mHardpoints.begin(), bugImage.mHardpoints.end(), [this](std::shared_ptr<CHardpoint> hardpoint) { AddHardpoint(hardpoint); });
}

/**
 * Constructor.
 */
CBugImage::CBugImage()
{
}


/**
 * Destructor.
 */
CBugImage::~CBugImage()
{
}

/**
 * Moves the left-center point of the image to a new location.
 * \param point The new location of the left-center point of the image.
 */
void CBugImage::SetCenterPoint(Gdiplus::PointF point)
{
	if (!point.Equals(mCenterPoint))
	{
		
		point = point - mCenterPoint;
		mCenterPoint = mCenterPoint + point;
		UpdateChildren(point);
	}
	
}


/**
 * Sets the parent point of this image.
 * \param hardPoint The new parent hardpoint of this image.
 */
void CBugImage::SetParentPoint(CHardpoint * hardPoint)
{
	if (mParentPoint != nullptr)
		mParentPoint->RemoveChild(this);
	mParentPoint = hardPoint;

}

/**
 * Add a hardpoint to the container of hardpoints.
 * \param hardpoint Pointer to the hardpoint to add.
 */
void CBugImage::AddHardpoint(std::shared_ptr<CHardpoint> hardpoint)
{
	mHardpoints.push_back(hardpoint);
	hardpoint->SetParent(this);
}

/**
 * Moves this image to its parent hardpoint, 
 * and then does the same for each of its children hardpoints.
 */
void CBugImage::Update()
{
	Gdiplus::PointF translate = mParentPoint->GetPosition() - mCenterPoint;

	mCenterPoint = mCenterPoint + translate;

	UpdateChildren(translate);
}

/**
 * Draw this image after applying temporary matrix transformations.
 * \param graphics The pointer to the graphics object.
 */
void CBugImage::DrawImage(Gdiplus::Graphics * graphics)
{

	Gdiplus::GraphicsState state = graphics->Save();
	graphics->TranslateTransform((float)mCenterPoint.X, (float)mCenterPoint.Y);
	
	graphics->RotateTransform((float)(mRotation * RtoD));
	Gdiplus::Rect rect;
	if (!mMirror)
	{
		rect = Gdiplus::Rect(
			0, 
			-(int)(GetCenter().Y * mScale), 
			(int)(mImage->GetWidth() * mScale), 
			(int)(mImage->GetHeight() * mScale));
	}
	else
	{
		rect = Gdiplus::Rect( 
			0, 
			-(int)(GetCenter().Y * (float)mScale),
			(int)(mImage->GetWidth() * -mScale), 
			(int)(mImage->GetHeight() * mScale));
	}
	graphics->DrawImage(mImage.get(), rect);
	graphics->Restore(state);


	for (std::shared_ptr<CHardpoint> hardpoint : mHardpoints)
	{
		hardpoint->Draw(graphics);

	}
	
}

/**
 * Set the image that will be drawn by this object.
 * \param filename Filename of the image to load.
 */
void CBugImage::SetImage(const wchar_t *filename)
{
	mFilename = filename;
	mImage = std::unique_ptr<Gdiplus::Bitmap>(Gdiplus::Bitmap::FromFile(filename));
	if (mImage->GetLastStatus() != Gdiplus::Ok)
	{
		std::wstring msg(L"Failed to open ");
		msg += filename;
		AfxMessageBox(msg.c_str());
	}
}

/**
 * Applies a rotation to the bug.
 * \param rotation Angle to add to rotation angle in radians.
 */
void CBugImage::Rotate(double rotation)
{
	SetRotation(mRotation + rotation);
	for (std::shared_ptr<CHardpoint> hardpoint : mHardpoints)
	{
		hardpoint->RotateAround(rotation, mCenterPoint);
	}
}

/**
 * Called when the frame of the bug is set.
 * \param frame New frame of the bug that owns this.
 */
void CBugImage::SetFrame(int frame)
{
	for (std::shared_ptr<CHardpoint> hardpoint : mHardpoints)
	{
		
	}
}

/**
 * Make the children of this image accept the visitor.
 * \param visitor Pointer to the BodyPartVisitor visiting the bug.
 */
void CBugImage::AcceptBodyPartVisitor(CBodyPartVisitor * visitor)
{
	for (std::shared_ptr<CHardpoint> hardpoint : mHardpoints)
	{
		for (std::vector<std::shared_ptr<CBugImage> >::iterator child = hardpoint->Begin();
			child != hardpoint->End();
			++child)
		{
			child->get()->AcceptBodyPartVisitor(visitor);
		}

	}
}

/**
 * Gets the centerpoint of the image.
 * \returns The point at the center of the image. 
 */
Gdiplus::PointF CBugImage::GetImageCenter()
{
	if (mImage != nullptr)
	{
		return Gdiplus::PointF(
			float(mImage->GetWidth() / 2),
			float(mImage->GetHeight() / 2)
		);
	}
	return Gdiplus::PointF(0, 0);
}

/**
 * Update the hardpoints of the image, moving them if the
 * image has moved.
 * \param translate Translation transformation that the image has gone through.
 */
void CBugImage::UpdateChildren(Gdiplus::PointF translate)
{
	for (std::shared_ptr<CHardpoint> hardpoint : mHardpoints)
	{
		hardpoint->Translate(translate);
	}
}


