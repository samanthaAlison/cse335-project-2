/**
 * \file HarvestVisitor.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "HarvestVisitor.h"
#include "HeadActual.h"
#include "BasketActual.h"

/**
 * Constructor.
 * \param basket Pointer to the basket that the heads will be harvested in. 
 */
CHarvestVisitor::CHarvestVisitor(std::shared_ptr<CBasketActual> basket) : mBasket(basket)
{
	mHead = nullptr;
}




/**
* Destructor.
*/
CHarvestVisitor::~CHarvestVisitor()
{


}

/**
* Visit a body part of a bug.
* \param bodyPart Pointer to the body part we are visiting.
*/
void CHarvestVisitor::VisitBodyPart(CBodyPart * bodyPart)
{
}

/**
* Visit a leg of a bug.
* \param leg Pointer to the leg we are visiting.
*/
void CHarvestVisitor::VisitLeg(CLeg * leg)
{
}

/**
* Visit a segment of a bug. Bends the segment.
* \param segment Pointer to the segment we are visiting.
*/
void CHarvestVisitor::VisitSegment(CSegment * segment)
{
}

/**
 * Visits a head of a bug. Harvest the head if it's being drawn.
 * \param head Pointer to the head we are visiting
 */
void CHarvestVisitor::VisitHead(CHeadActual * head)
{
	if (head->GetScale() > 0.000001)
	{
		mHead = head->GetSelf();
		std::shared_ptr<CBasketActual> basket = mBasket.lock();
		basket->AddHeadToBasket(mHead);
		
	}
}

/**
* Visit a stinger of a bug. Bends the stinger.
* \param stinger Pointer to the stinger we are visiting.
*/
void CHarvestVisitor::VisitStinger(CStinger * stinger)
{
}
