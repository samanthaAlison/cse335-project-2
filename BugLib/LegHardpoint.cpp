/**
 * \file LegHardpoint.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "LegHardpoint.h"
#include  "Segment.h"
#include "Leg.h"


/// Value of Pi.
const double Pi = 3.14;


/**
 * Constructor.
 * \param part The body part that will be the parent of this point.
 * \param filename File name of the image for the legs.
 */
CLegHardpoint::CLegHardpoint(CSegment* part, wchar_t * const * filename) : CBugHardpoint(part), mFilename(filename)
{
	Spawn();
}


/**
 * Destructor.
 */
CLegHardpoint::~CLegHardpoint()
{
}


/**
 * Initiates the spawning of this hardpoint's children. Will grow two legs of the same image.
 */
void CLegHardpoint::Spawn()
{
	int startGrow = GetStartGrow();
	std::shared_ptr<CLeg> leg1 = std::make_shared<CLeg>(GetBug(), *mFilename, startGrow);
	std::shared_ptr<CLeg> leg2 = std::make_shared<CLeg>(GetBug(), *mFilename, startGrow);
	leg2->SetEndGrow(leg1->GetEndGrow());
	leg2->SetRotation(-Pi / 7.0);
	AddChild(leg1);
	AddChild(leg2);
	FinalizeAdd();
	Rotate(Pi / 2.0);
}
