/**
 * \file HarvestVisitor.h
 *
 * \author Samantha Oldenburg
 *
 * Visitor class that harvests the heads of a bug.
 */

#pragma once
#include "BodyPartVisitor.h"

#include <memory>
class CHead;
class CBasketActual;


/**
 * Visitor class that harvests the heads of a bug.
 */
class CHarvestVisitor :
	public CBodyPartVisitor
{
public:
	CHarvestVisitor(std::shared_ptr<CBasketActual> basket);
	virtual ~CHarvestVisitor();


	virtual void VisitBodyPart(CBodyPart * bodyPart);
	virtual void VisitLeg(CLeg * leg);
	virtual void VisitSegment(CSegment * segment);
	virtual void VisitHead(CHeadActual * head);
	virtual void VisitStinger(CStinger * stinger);

	/** Get the pointer to the head. It will be nullptr if
	 * no head has been found yet. 
	 * \returns A pointer to a head or nullptr. */
	std::shared_ptr<CHeadActual> GetHead() { return mHead; }

private:

	/// Pointer to a head that will be harvested.
	std::shared_ptr<CHeadActual> mHead;

	/// Pointer to a basket.
	std::weak_ptr<CBasketActual> mBasket;
};

