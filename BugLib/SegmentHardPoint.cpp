/**
 * \file SegmentHardPoint.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "SegmentHardPoint.h"
#include "Segment.h"
#include "BugImage.h"
#include "BugActual.h"
#include "PseudoRandom.h"
#include "HeadActual.h"
#include "Stinger.h"

/**
 * Constructor.
 * \param part The bodypart that owns this hardpoint
 * \param left Whether this hardpoint will spawn left or right segments.
 */
CSegmentHardpoint::CSegmentHardpoint(CBodyPart * part, bool left) : CBugHardpoint(part), mLeft(left)
{
	Spawn();
}

/**
 * Destructor.
 */
CSegmentHardpoint::~CSegmentHardpoint()
{
}

//
// IN ORDER TO USE THE PLAY FROM START FEATURE, NEED TO REST
// BUG SOME WAY
//

/**
 * Spawns body parts.
 * 
 * A segment hardpoint will only spawn a segment, or either a head
 * a stinger depending on it's mLeft value.
 */
void CSegmentHardpoint::Spawn()
{
	int startGrow = GetStartGrow();
	int frame = GetBug()->GetFrame();
	if (ItemRemoved())
	{
		startGrow = frame + GetRandom()->Random(5, 15);
	}

	int headChance = GetRandom()->Random(0, 3);
	if (headChance == 3)
	{
		if (mLeft)
		{
			std::shared_ptr<CHeadActual> head = std::make_shared<CHeadActual>(GetBug(), mLeft, startGrow);
			head->SetSelf(head);
			AddChild(head);
		}
		else
		{
			AddChild(std::make_shared<CStinger>(GetBug(), startGrow));
		}
	}
	else
	{
		AddChild(std::make_shared<CSegment>(GetBug(), mLeft, startGrow));
	}
}


