/**
 * \file Stinger.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "Stinger.h"

/// Amount of stinger images
const int StingerImageCount = 2;

/// Formate for stinger image file names
const wchar_t * StingerImageFormat = L"images/stinger%i.png";

/**
 * Constructor.
 * \param bug Bug that owns this.
 * \param startGrow Frame to start growing this at. 
 */
CStinger::CStinger(CBugActual * bug, int startGrow) : CBodyPart(bug, startGrow)
{
	GetImageFile(StingerImageFormat, StingerImageCount);
}


/**
 * Destructor.
 */
CStinger::~CStinger()
{
}
