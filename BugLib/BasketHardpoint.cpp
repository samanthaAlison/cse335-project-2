#include "stdafx.h"
#include "BasketHardpoint.h"
#include "Head.h"

/**
 * Constructor.
 * \param parent Image that owns this hardpoint. 
 */
CBasketHardpoint::CBasketHardpoint(CBugImage * parent) : CHardpoint(parent)
{
}


/**
 * Destructor.
 */
CBasketHardpoint::~CBasketHardpoint()
{
}


/**
 * Adds a head to the collection of heads.
 * \param head Pointer to the head to add.
 */
void CBasketHardpoint::AddHead(std::shared_ptr<CHead> head)
{
	head->SetBasketPosition((int)GetPosition().X, (int)GetPosition().Y);
	mHeads.push_back(head);
}


/**
 * Draws the harvested heads in the basket.
 * \param graphics Pointer to the graphics object.
 */
void CBasketHardpoint::Draw(Gdiplus::Graphics * graphics)
{
	Update();
	for (std::shared_ptr<CHead> head : mHeads)
	{
		head->SetBasketPosition((int)GetPosition().X, (int)GetPosition().Y);
	}
	CHardpoint::Draw(graphics);
}

/**
 * Spawn function for the basket class. Does nothing, as the basket
 * doesn't spawn any body parts.
 */
void CBasketHardpoint::Spawn()
{

}


/**
 * Completely empties the basket.
 */
void CBasketHardpoint::Clear()
{
	mHeads.clear();
	for (std::vector<std::shared_ptr<CBugImage> >::iterator child = Begin(); child != End(); ++child)
	{
		RemoveChild(child->get());
	}
}
