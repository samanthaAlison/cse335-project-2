/**
 * \file BendVisitor.h
 *
 * \author Samantha Oldenburg
 *
 * Visitor class that will bend heads, segments, and stingers.
 */

#pragma once
#include "BodyPartVisitor.h"
/**
 * Visitor class that will bend heads, segments, and stingers.
 */
class CBendVisitor :
	public CBodyPartVisitor
{
public:
	CBendVisitor(double bend);
	virtual ~CBendVisitor();

	virtual void VisitBodyPart(CBodyPart * bodyPart);
	virtual void VisitLeg(CLeg * leg);
	virtual void VisitSegment(CSegment * segment);
	virtual void VisitHead(CHeadActual * head);
	virtual void VisitStinger(CStinger * stinger);
private:

	/// Angle to rotate each segment at. 
	double mBend;
};

