/**
 * \file SegmentHardPoint.h
 *
 * \author Samantha Oldenburg
 *
 * A type of hardpoint that can grow a segment, 
 * head or stinger.
 */

#pragma once
#include "BugHardpoint.h"
class CBodyPart;


/**
 * A type of hardpoint that can grow a segment, 
 * head or stinger.
 */
class CSegmentHardpoint :
	public CBugHardpoint
{
public:
	CSegmentHardpoint(CBodyPart *part, bool left);
	virtual ~CSegmentHardpoint();



	virtual void Spawn();

private:
	/// True if the hardpoint belongs to a segment on the
	/// left side of the bug. 
	bool mLeft;
};

