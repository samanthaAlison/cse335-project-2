/**
 * \file BasketActual.h
 *
 * \author Samantha Oldenburg
 *
 * Describes a class that represents a basket that collects the 
 * bug heads.
 */

#pragma once
#include "Basket.h"
#include "BasketHardpoint.h"
#include "BugImage.h"
class CHeadActual;

/**
 * Class that represents a basket that collects the 
 * bug heads.
 */
class CBasketActual :
	public CBasket
{
public:
	CBasketActual();
	virtual ~CBasketActual();

	/**
	* Draw the basket at the currently specified location
	* \param graphics GDI+ Graphics object
	*/
	virtual void DrawBasket(Gdiplus::Graphics *graphics) override;

	/**
	* Add head to basket
	* \param head Head object
	*/
	virtual void AddToBasket(std::shared_ptr<CHead> head) override;

	
	virtual void EmptyBasket() override;

	void  AddHeadToBasket(std::shared_ptr<CHeadActual> head);

private:

	/// Image of the back of the basket.
	CBugImage mBackBasket;

	/// Image of the front of the basket.
	CBugImage mFrontBasket;

	
	/// Pointer to a special hardpoint used by the basket.
	std::shared_ptr<CBasketHardpoint> mHardpoint;
};

