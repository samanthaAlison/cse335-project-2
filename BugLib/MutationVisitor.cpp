/**
 * \file MutationVisitor.cpp
 *
 * \author Samantha Oldenburg
 */


#include "stdafx.h"
#include "MutationVisitor.h"


/**
 * Constructor. 
 * \param random Pointer to random number generator 
 * \param probability Probability of mutation occuring
 * \param allowed Mutations allowed to occur.
 */
CMutationVisitor::CMutationVisitor(CPseudoRandom * random, double probability, int allowed) : mProbability(probability), mOccurencesAllowed(allowed), mRandom(random)
{
}



/**
 * Destructor.
 */
CMutationVisitor::~CMutationVisitor()
{

	
}

/**
 * Visit a body part of a bug.
 * \param bodyPart Pointer to the body part we are visiting.
 */
void CMutationVisitor::VisitBodyPart(CBodyPart * bodyPart)
{
}

/**
* Visit a leg of a bug.
* \param leg Pointer to the leg we are visiting.
*/
void CMutationVisitor::VisitLeg(CLeg * leg)
{
}

/**
* Visit a segment of a bug. Bends the segment.
* \param segment Pointer to the segment we are visiting.
*/
void CMutationVisitor::VisitSegment(CSegment * segment)
{
	if (segment->GetParentPoint() != nullptr)
	{
		segment->Update();
		segment->GetParentPoint()->Update();
		if (mRandom->Random(0.0, 1.0) < mProbability && mOccurencesAllowed > 0)
		{
			std::shared_ptr<CMutatedSegment> mutatedSegment =
				std::make_shared<CMutatedSegment>(*segment);
			segment->Delete();

			mutatedSegment->GetParentPoint()->AddChild(mutatedSegment);
			//mutatedSegment->GetParentPoint()->Update();
			mOccurencesAllowed--;
		}
	}
}


/**
* Visit a head of a bug. Bends the head.
* \param head Pointer to the head of the bug we are visiting.
*/
void CMutationVisitor::VisitHead(CHeadActual * head)
{
}

/**
* Visit a stinger of a bug. Bends the stinger.
* \param stinger Pointer to the stinger we are visiting.
*/
void CMutationVisitor::VisitStinger(CStinger * stinger)
{
	
}
