/**
 * \file BugFactory.cpp
 *
 * \author Charles Owen
 * \author Samantha Oldenburg
 *
 * You are allowed to change this file.
 */

#include "stdafx.h"
#include "BugFactory.h"
#include "Bug.h"
#include "Basket.h"
#include "BugActual.h"
#include "BasketActual.h"
#include "HarvestVisitor.h"
using namespace std;


std::weak_ptr<CBasketActual> CBugFactory::mBasketPointer;

/**
 * Constructor
 */
CBugFactory::CBugFactory()
{

}


/**
 * Destructor
 */
CBugFactory::~CBugFactory()
{
}


/**
 * Create a bug object
 * \returns Object of type CBug
 */
std::shared_ptr<CBug> CBugFactory::CreateBug()
{
    std::shared_ptr<CBugActual> bug =  make_shared<CBugActual>();
	return bug;
}


/**
 * Create a Basket object
 * \returns Object of type CBasket
 */
std::shared_ptr<CBasket> CBugFactory::CreateBasket()
{
	std::shared_ptr<CBasketActual> basket = std::make_shared<CBasketActual>();
	if (!mBasketMade)
	{
		mBasketPointer = basket;
	}
	return basket;
}

/**
 * Creates a HarvestVisitor to harvest the bug.
 * \returns A pointer to the new HarvestVisitor.
 */
std::shared_ptr<CHarvestVisitor> CBugFactory::CreateHarvester()
{
	std::shared_ptr<CHarvestVisitor> harvester = std::make_shared<CHarvestVisitor>(mBasketPointer.lock());

	return harvester;
}
