/**
 * \file BugFactory.h
 *
 * \author Charles Owen
 *
 * Bug factory class. Creates Bug and Basket objects
 */

#pragma once

#include <memory>

class CBug;
class CBasket;
class CBasketActual;
class CHarvestVisitor;
/**
 * Bug factory class. Creates Bug and Basket objects
 *
 * AFX_EXT_CLASS is a Microsoft directive that indicates
 * that this class can be used outside the DLL. Do not change
 * it or add it to any other classes.
 */
class AFX_EXT_CLASS CBugFactory
{
public:
    CBugFactory();
    virtual ~CBugFactory();

    std::shared_ptr<CBug> CreateBug();
    std::shared_ptr<CBasket> CreateBasket();
	std::shared_ptr<CHarvestVisitor> CreateHarvester();

private:

	/// True if a basket has been created
	bool mBasketMade = false;

	/// Pointer to the first basket created by a instance of this factory.
	/// Harvesting visitors use this pointer to interact with the basket. 
	static std::weak_ptr<CBasketActual> mBasketPointer;
};

