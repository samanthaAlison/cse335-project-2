/**
 * \file Leg.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "Leg.h"
#include "BugActual.h"

/**
 * Constructor. 
 * \param bug The BugActual object that will own this.
 * \param filename The filename of the leg image to use.
 * \param startGrow The frame to start growing this part at.
 */
CLeg::CLeg(CBugActual * bug, const wchar_t *  filename, int startGrow) : CBodyPart(bug, startGrow)
{
	SetImage(filename);
}


/**
 * Destructor. 
 */
CLeg::~CLeg()
{
}
