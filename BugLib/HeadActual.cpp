/**
 * \file HeadActual.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "HeadActual.h"
#include "BodyPartVisitor.h"
#include "BugActual.h"
#include "BugHardpoint.h"

/// Amount of head image files
const int HeadImageCount = 3;
/// File format for rightward facing heads
const wchar_t * HeadImageFormat = L"images/head%i.png";

/// File fomrat for leftward facing heads
const wchar_t * HeadLeftImageFormat = L"images/head%im.png";

/// Amount of eye image files
const int EyeImageCount = 3;

/// Format for eye image file name.
const wchar_t * EyeImageFormat = L"images/eye%i.png";


/**
 * Constructor. Loads head image. Spawns a random amount of eyes.
 * \param bug Bug that owns this head indirectly.
 * \param left Determines if the head is on the left or right side.
 * \param startGrow Frame at which to start growing the bug.
 */
CHeadActual::CHeadActual(CBugActual * bug, bool left, int startGrow) : CBodyPart(bug, startGrow)
{
	SetMirror(left);
	if (left)
	{
		GetImageFile(HeadLeftImageFormat, HeadImageCount);
	}
	else
	{
		GetImageFile(HeadImageFormat, HeadImageCount);
	}



	//
	// Spawn the eyes.
	//
	int eyeCount = GetBug()->GetRandom()->Random(1, 5);
	wchar_t filename[32];
	GetImageFileName(filename, EyeImageFormat, EyeImageCount);
	while (eyeCount > 0)
	{
		// Make a hardpoint for the eye.
		std::shared_ptr<CBugHardpoint> hardpoint = std::make_shared<CBugHardpoint>(this);
		// Make the eye.
		std::shared_ptr<CBugImage> eye = std::make_shared<CBodyPart>(GetBug(),
			GetStartGrow() + GetBug()->GetRandom()->Random(5, 15));
		eye->SetImage(filename);
		

		// Make the eye a child of the hardpoint.
		hardpoint->AddChild(eye);
		
		// Make the hardpoint a child of this head.
		AddHardpoint(hardpoint);

		int x = GetBug()->GetRandom()->Random(0, GetImageWidth());
		int y = GetBug()->GetRandom()->Random(0, GetImageHeight());
		hardpoint->SetPosition(Gdiplus::PointF(-(float)x, (float)y));
		eyeCount--;

	}


}


/**
 * Destructor.
 */
CHeadActual::~CHeadActual()
{

}


/**
 * Moves this head into the basket.
 * \param x X coordinate of the basket.
 * \param y Y coordinate of the basket.
 */
void CHeadActual::SetBasketPosition(int x, int y)
{
	SetCenterPoint(Gdiplus::PointF((float)x, (float)y));
}

/**
 * Draws the bug in the basket. The bug has already been moved to the basket,
 * so this simply calls the draw function for the CBugImage class.
 * \param graphics Pointer to the graphics object.
 */
void CHeadActual::DrawInBasket(Gdiplus::Graphics * graphics)
{
	DrawImage(graphics);
}


/**
 * Accepts a body part visitor. Tells the visitor this object is a head.
 * \param visitor Visitor to be accepted.
 */
void CHeadActual::AcceptBodyPartVisitor(CBodyPartVisitor * visitor)
{
	visitor->VisitHead(this);
	CBugImage::AcceptBodyPartVisitor(visitor);
}
