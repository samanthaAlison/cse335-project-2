/**
 * \file Leg.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes a leg of a bug. 
 */

#pragma once
#include "BodyPart.h"

/**
 * Class that describes a leg of a bug.
 */
class CLeg :
	public CBodyPart
{
public:
	CLeg(CBugActual * bug, const wchar_t * filename, int startGrow);
	virtual ~CLeg();
};

