/**
 * \file Segment.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"

#include "Segment.h"
#include "SegmentHardPoint.h"
#include "BugActual.h"
#include "LegHardpoint.h"
#include "BodyPartVisitor.h"
#include <string>
#include <cstdio>
#include <cstdlib>
/// Amount of segment image files
const int SegmentImageCount = 5;

/// Format for Segment file names
const wchar_t * SegmentImageFormat = L"images/segment%i.png";

/// Format for Leg file names
const wchar_t * LegImageFormat = L"images/leg%i.png";

/// Amount of leg image files.
const int LegImageCount(3);

///Spacing between segments.
const float SegmentSpacing = -5.0f;



/**
 * Copy constructor.
 * \param segment Segment we are copying.
 */
CSegment::CSegment(const CSegment & segment) : CBodyPart(segment)
{
	mLegImage = new wchar_t[32];
	memcpy(mLegImage, segment.mLegImage, sizeof(wchar_t)*32);
	mLeft = segment.mLeft;
}


/**
 * Constructor.
 * \param bug Bug that owns this segment
 * \param left Whether the segment is on the left or right side of the bug.
 * \param startGrow Frame to start growing the bug at. 
 */
CSegment::CSegment(CBugActual * bug, bool left, int startGrow) : CBodyPart(bug, startGrow), mLeft(left)
{
	SetMirror(mLeft);
	GetImageFile(SegmentImageFormat, SegmentImageCount);
	std::shared_ptr<CSegmentHardpoint> segmentHardpoint = std::make_shared<CSegmentHardpoint>(this, left);
	if (mLeft)
	{
		segmentHardpoint->SetPosition(Gdiplus::PointF(-1 * (GetImageWidth() + SegmentSpacing), GetCenterPoint().Y));

	}
	else
	{
		segmentHardpoint->SetPosition(Gdiplus::PointF((GetImageWidth() + SegmentSpacing), GetCenterPoint().Y));

	}
	mLegImage = new wchar_t[32];
	GetImageFileName(mLegImage, LegImageFormat, LegImageCount);
	AddHardpoint(segmentHardpoint);
	segmentHardpoint->RotateAround(GetRotation(), GetCenterPoint());

	CPseudoRandom * random = bug->GetRandom();
	int legHardpointCount = random->Random(0, 3);
	int i = legHardpointCount;
	int dir = 1;
	if (mLeft)
		dir = -1;
	while (i > 0)
	{
		std::shared_ptr<CLegHardpoint> legHardpoint = std::make_shared<CLegHardpoint>(this, &mLegImage);
		legHardpoint->SetPosition(Gdiplus::PointF(
			((float)dir*(GetImageWidth()) * i / (LegImageCount + 1)),
			(float)GetCenter().Y + 15));
		AddHardpoint(legHardpoint);
		i--;
	}

}


/**
 * Destructor.
 */
CSegment::~CSegment()
{
	delete mLegImage;
}


/**
 * Accepts a body part visitor.
 * \param visitor Visitor to accept.
 */
void CSegment::AcceptBodyPartVisitor(CBodyPartVisitor * visitor)
{
	visitor->VisitSegment(this);
	CBugImage::AcceptBodyPartVisitor(visitor);
}



