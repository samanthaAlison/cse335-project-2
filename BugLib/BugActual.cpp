/**
 * \file BugActual.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "BugActual.h"
#include "BodyPart.h"
#include "RootSegment.h"
#include "HarvestVisitor.h"
#include "BugFactory.h"
#include "HeadActual.h"
#ifdef _MUTATION
#include "MutationVisitor.h"
#endif // _MUTATION

#include "BodyPart.h"

/**
 * Constructor.
 */
CBugActual::CBugActual()
{
	mRandom = CPseudoRandom();

	mRoot = std::make_shared<CRootSegment>(this, 1);
	mRoot->Update();

#ifdef _MUTATION
	CMutationVisitor mutant(&mRandom, 0.5, 5);
	mRoot->AcceptBodyPartVisitor(&mutant);

#endif // _MUTATION

	
}


/**
 * Destroyer.
 */
CBugActual::~CBugActual()
{
	
}


/**
 * Set the frame of the bug. The frame represents the age.
 * \param frame The new frame of the bug.
 */
void CBugActual::SetBugFrame(int frame)
{

	mFrame = frame;
	if (mFrame == 0)
	{
		SetSeed(mSeed);
	}
	mRoot->Update();
}



/**
 * Draws  the bug on the screen.
 * \param graphics Pointer to the graphics object
 */
void CBugActual::DrawBug(Gdiplus::Graphics * graphics)
{
	mRoot->Update();
	mRoot->DrawImage(graphics);
}

/**
 * Set the seed of the bug and regrow it.
 * \param seed The seed to ser the bug's random generator to
 */
void CBugActual::SetSeed(int seed)
{
	mSeed = seed;
	mRandom.Seed(seed);
	mRoot = std::make_shared<CRootSegment>(this, 1);
	mRoot->Update();
#ifdef _MUTATION
	CMutationVisitor mutant(&mRandom, 0.5, 5);
	mRoot->AcceptBodyPartVisitor(&mutant);

#endif // _MUTATION


}


/**
 * Harvest a head of the bug.
 * \returns The head that will be harvested, or 
 * nullptr if nothing was found.
 */
std::shared_ptr<CHead> CBugActual::Harvest()
{
	CBugFactory factory;
	std::shared_ptr<CHarvestVisitor> harvester = factory.CreateHarvester();
	mRoot->AcceptBodyPartVisitor(harvester.get());
	
	std::shared_ptr<CHead> head = harvester->GetHead();
	return head;
}


/**
 * Sets the bend parameter of the bug, bending the segments
 * \param bend The new bend parameter of the bug in radians 
 * (will be divided by 10 in the function).
 */
void CBugActual::SetBend(double bend)
{
	mRoot->SetBend(bend / 10.0);
}

/** Gets the seed the bug used to grow. 
 * \returns The seed of the bug's random generator.*/
int CBugActual::GetSeed()
{
	return mSeed;
}
