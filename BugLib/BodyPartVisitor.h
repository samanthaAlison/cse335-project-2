/**
 * \file BodyPartVisitor.h
 *
 * \author Samantha Oldenburg
 *
 * Visitor class to use with body parts.
 */

#pragma once
class CBodyPart;
class CLeg;
class CSegment;
class CHeadActual;
class CStinger;

/**
 * Visitor class to use with body parts.
 */
class CBodyPartVisitor
{
public:
	CBodyPartVisitor();
	virtual ~CBodyPartVisitor();
	/** Visit a body part of a bug.
	* \param bodyPart Pointer to the body part we are visiting. */
	virtual void VisitBodyPart(CBodyPart * bodyPart) = 0;
	/** Visit a leg of a bug.
	 * \param leg Pointer to the leg we are visiting. */
	virtual void VisitLeg(CLeg * leg) = 0;
	/**
	* Visit a segment of a bug.
	* \param segment Pointer to the segment we are visiting. */
	virtual void VisitSegment(CSegment * segment) = 0;
	/**
	* Visit a head of a bug
	* \param head Pointer to the head of the bug we are visiting. */
	virtual void VisitHead(CHeadActual * head) = 0;
	/** Visit a stinger of a bug.
	 * \param stinger Pointer to the stinger we are visiting.*/
	virtual void VisitStinger(CStinger * stinger) = 0;

};

