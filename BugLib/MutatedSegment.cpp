#include "stdafx.h"
#include "MutatedSegment.h"
#include "Segment.h"
#include "BugActual.h"
#include "Hardpoint.h"

/**
 * Changeling Constructor. Copy constructor that occurs before 
 * replacing an object.
 * \param segment Segment that is the target of the changeling. 
 */
CMutatedSegment::CMutatedSegment(const CSegment &segment) : CSegment(segment)
{
	SetCenterPoint(GetParentPoint()->GetPosition());
	Mutate();
}

/**
 * Destructor.
 */
CMutatedSegment::~CMutatedSegment()
{
}


/**
 * Adds mutation to segment that gives it another segment.
 */
void CMutatedSegment::Mutate()
{
	
	std::shared_ptr<CSegmentHardpoint> hardpoint = std::make_shared<CSegmentHardpoint>(this, IsLeft());
	AddHardpoint(hardpoint);
	hardpoint->SetPosition(Gdiplus::PointF((GetCenterPoint().X - GetImageWidth() * 0.5f), GetCenterPoint().Y));

	Update();
	hardpoint->Rotate(-(double(GetBug()->GetRandom()->Random(0.0, 3.14))) / 2.0);
}


