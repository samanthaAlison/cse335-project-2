/**
 * \file BugHardpoint.h
 *
 * \author Samantha Oldenburg
 * 
 * Class that describes a type of Hardpoint 
 * that is owned by a CBugActual object. 
 */

#pragma once
#include "Hardpoint.h"
class CBodyPart;
class CBugActual;
class CPseudoRandom;
/**
 * Class that describes a type of Hardpoint 
 * that is owned by a CBugActual object.
 */
class CBugHardpoint :
	public CHardpoint
{
public:
	CBugHardpoint(CBodyPart * part);
	virtual ~CBugHardpoint();
	/**Spawns any body parts. This hardpoint is 
	 * destined to spawn. */
	virtual void Spawn() {};


protected:
	CPseudoRandom * GetRandom();
	/** Get the frame at which the parent will start 
	 * to grow
	 * \returns The StartGrowFrame of the parent. */
	int	GetParentStart() { return mParentStart; }
	/** Get the frame at which the parent will stop
	 * growing
	 * \returns The EndGrowFrame of the parent. */
	int GetParentEnd() { return mParentEnd; }
	int GetStartGrow();
	/** Get the bug that indirectly owns this hardpoint
	 * \returns A pointer to the CBugActual object.*/
	CBugActual * GetBug() { return mBug; }
private:

	/// The frame at which the parent will start to grow.
	int mParentStart = 0;

	/// The frame at which this hardpoint's parent will stop growing.
	int mParentEnd = 0;

	/// Pointer to the bug that indirectly owns this hardpoint.
	CBugActual *  mBug;
	
};

