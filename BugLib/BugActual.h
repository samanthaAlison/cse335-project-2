/**
 * \file BugActual.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes a mutant bug.
 *
 * The mutant bug has different body parts, and
 * can be drawn on screen with GDI+.
 */

/// Enables mutant body parts.
#define _MUTATION

#pragma once
#include "Bug.h"
#include "PseudoRandom.h"
#include <memory>



class CRootSegment;
/**
 * Class that describes a mutant bug.
 */
class CBugActual :
	public CBug
{
public:
	/** \brief Copy constructor disabled */
	CBugActual(const CBugActual &) = delete;
	/** \brief Assignment operator disabled */
	void operator=(const CBugActual &) = delete;
	CBugActual();
	virtual ~CBugActual();

	/** Get the random generator of the bug
	* \returns A pointer to the random generator of the bug. */
	CPseudoRandom *GetRandom() { return &mRandom; }

	virtual void SetBugFrame(int frame) override;

	virtual void DrawBug(Gdiplus::Graphics * graphics) override;

	virtual void SetSeed(int seed) override;

	/** Get the current frame of the bug
	 * \returns The frame of the bug. */
	int GetFrame() { return mFrame; }

	virtual std::shared_ptr<CHead> Harvest() override;

	virtual void SetBend(double bend) override;

	virtual int GetSeed() override;

private:

	/// A random number generator used to generate the bug.
	CPseudoRandom mRandom;

	/// The current frame of the bug.
	int mFrame;
	
	/// The bend parameter of the bug.
	double mBend = 0.0;
	
	/// Pointer to the root segment of this bug
	std::shared_ptr<CRootSegment> mRoot;

	/// Seed of the bug's random generator.
	int mSeed = 0;




};

