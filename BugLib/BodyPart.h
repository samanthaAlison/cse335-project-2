/**
 * \file BodyPart.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes a body part of a bug.
 *
 * Every drawable component of a bug is a body part.
 */

#pragma once
#include "stdafx.h"
#include "BugImage.h"

class CBodyPartVisitor;
class CBugActual;


/**
 * Class that describes a body part of a bug.
 */
class CBodyPart :
	public CBugImage
{
public:

	CBodyPart(const CBodyPart &bodyPart);
	CBodyPart(CBugActual * bug, int startGrow);
	virtual ~CBodyPart();


	void SetBug(CBugActual *bug);

	void GetImageFileName(wchar_t * filename, const wchar_t * format, int numImages);

	virtual void GetImageFile(const wchar_t * format, int numImages);

	/** Gets the bug that owns this part indirectly.
	 * \returns a pointer to the bug that owns this part. */
	CBugActual * GetBug() { return mBug; }

	
	virtual Gdiplus::PointF GetCenter();

	/** Sets the frame at which this bodypart will 
	 * stop growing.
	 * \param endGrow The new stop grow frame of this. */
	void SetEndGrow(int endGrow) { mEndGrow = endGrow; }
	
	/** Sets the frame at which this bodypart will
	* start growing.
	* \param startGrow The new frame this will start to grow at.*/
	void SetStartGrow(int startGrow) { mStartGrow = startGrow; }

	/** Gets the frame at which this bodypart will
	* start growing.
	* \returns The frame when this will start growing. */
	int GetStartGrow() { return mStartGrow; }
	/** Gets the frame at which this bodypart will
	* stop growing.
	* \returns The frame at which the scale of this is 1.0. */
	int GetEndGrow() { return mEndGrow; }

	virtual void SetFrame(int frame) override;

	virtual void AcceptBodyPartVisitor(CBodyPartVisitor * visitor);

	void Delete();
	
	virtual void Update() override;

	virtual void SetImage(const wchar_t * filename);

protected:
	void UpdateScale();
private:

	/// Pointer to the bug that indirectly owns this body part.
	CBugActual  *mBug;

	
	/// Frame to stop growing the bug at.
	int mEndGrow = 0;


	/// Frame to start growing the bug at.
	int mStartGrow = 0;

};


