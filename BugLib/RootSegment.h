/**
 * \file RootSegment.h
 *
 * \author Samantha Oldenburg
 *
 * Describes a class that represents the root segment of a bug.
 *
 * The root segment is the only access a bug has to its body parts. 
 */

#pragma once
#include "Segment.h"

/**
 *  Describes a class that represents the root segment of a bug.
 */
class CRootSegment :
	public CSegment
{
public:
	/** \brief Default constructor disabled */
	CRootSegment() = delete;

	CRootSegment(const CRootSegment &);
	
	/** \brief Assignment operator disabled */
	void operator=(const CRootSegment &) = delete;
	CRootSegment( CBugActual * bug, int startGrow);
	virtual ~CRootSegment();
	virtual void Update();

	virtual void SetBend(double bend);
	virtual void AcceptBodyPartVisitor(CBodyPartVisitor * visitor);
private:

	/// Holds the current bend angle of the bug in  radians.
	double mBend = 0.0;
};

