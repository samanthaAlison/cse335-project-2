/**
 * \file BasketActual.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "BasketActual.h"
#include <string>
#include "BugImage.h"
#include "Hardpoint.h"
#include "HeadActual.h"
#include "BasketHardpoint.h"

/// Filename for the basket image.
const wchar_t * BasketBackImage(L"images/basket.png");
/// Filename for the front of the basket's image.
const wchar_t * BasketFrontImage(L"images/basket-front.png");


/**
 * Constructor.
 */
CBasketActual::CBasketActual()
{
	mFrontBasket.SetImage(BasketFrontImage);
	mBackBasket.SetImage(BasketBackImage);

	mHardpoint = std::make_shared<CBasketHardpoint>(&mBackBasket);
	mHardpoint->SetPosition(Gdiplus::PointF(
		(float)(mBackBasket.GetImageWidth() / 2),
		(float)(mBackBasket.GetImageHeight() / 2)));
	mBackBasket.AddHardpoint(mHardpoint);

}


/**
 * Destructor.
 */
CBasketActual::~CBasketActual()
{
}

/**
* Draws this basket 
* \param graphics Pointer to graphics object.
*/
void CBasketActual::DrawBasket(Gdiplus::Graphics * graphics)
{
	mBackBasket.DrawImage(graphics);
	mFrontBasket.DrawImage(graphics);
}


/**
 * Adds a head to the basket
 * \param head 
 */
void CBasketActual::AddToBasket(std::shared_ptr<CHead> head)
{
	mHardpoint->AddHead(head);
}

/**
 * Empties the heads from the basket
 */
void CBasketActual::EmptyBasket()
{
	mHardpoint->Clear();
}


/**
 * Adds a head to this basket's hardpoint.
 * \param head Pointer to CHeadActual object to add to the basket.
 */
void CBasketActual::AddHeadToBasket(std::shared_ptr<CHeadActual> head)
{
	head->SetBug(nullptr);
	mHardpoint->AddChild(head);

}