/**
 * \file Segment.h
 *
 * \author Samantha Oldenburg
 *
 * Class that defines a segment of a bug's body.
 *
 * Segments can grow legs. They can also grow either a head, 
 * stinger, or another segment on one of their sides.
 */

#pragma once
#include "stdafx.h"
#include "BodyPart.h"

/**
 * Class that defines a segment of a bug's body.
 */
class CSegment :
	public CBodyPart
{
public:
	CSegment(const CSegment &segment);

	CSegment(CBugActual * bug, bool left, int startGrow);
	virtual ~CSegment();


	/** Gets whether the segment is to the left of the bug.
	 * \returns True if the segment is to the left of the bug. */
	bool IsLeft() { return mLeft; }
	
	virtual void AcceptBodyPartVisitor(CBodyPartVisitor * visitor);

private:

	/// True if the segment is on the left side of the bug.
	bool mLeft;

	/// Filename of the image used for the legs of this segment.
	wchar_t * mLegImage;
};

