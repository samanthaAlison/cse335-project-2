/**
 * \file Hardpoint.h
 *
 * \author Samantha Oldenburg
 *
 * Class that controls the location of its children BugImages.
 *
 * Hardpoints are intermediate classes that represent a BugImages
 * associations to its children BugImages. 
 */

#pragma once
#include "stdafx.h"
#include <vector>
#include <memory>


class CBugImage;
/**
 * Class that controls the location of its children BugImages.
 */
class CHardpoint
{
public:
	CHardpoint(CBugImage * parent);
	virtual ~CHardpoint();


	void AddChild(std::shared_ptr<CBugImage> child);
	/** Gets the parent BugImage of this hardpoint.
	 * \returns A pointer to the parent of this hardpoint.*/
	CBugImage * GetParent() { return mParent; }

	void SetPosition(Gdiplus::PointF point); 
	Gdiplus::PointF GetPosition();
	
	void SetParent(CBugImage * image);
	
	
	/** Get an iterator that points to the start of the
	* BugImage container
	* \returns The begin() iterator of the BugImage container. */
	std::vector<std::shared_ptr<CBugImage> >::iterator Begin() { return mChildren.begin(); }
	
	/** Get an iterator that points to the end of the
	* BugImage container
	* \returns The end() iterator of the BugImage container. */
	std::vector<std::shared_ptr<CBugImage> >::iterator End() { return mChildren.end(); }

	virtual void Rotate(double rotate);

	static double GetAngleOfPoints(Gdiplus::PointF pointA, Gdiplus::PointF pointB);
	
	void RotateAround(double theta, Gdiplus::PointF point);
	/** Adds a vector to the position of this point
	 * \param point Vector to add to position vector (point addition).*/
	void Translate(Gdiplus::PointF point) { mPosition = mPosition + point; Update(); }
	virtual void Update();
	

	/**Spawns body parts. */
	virtual void Spawn() = 0;
	void RemoveChild(CBugImage * image);
	virtual void Draw(Gdiplus::Graphics * graphics);



protected:
	void FinalizeRemove();
	void FinalizeAdd();
	/** Gets wheter an item has been removed since last update call
	 * \returns True if an item is in the to-remove container.*/
	bool ItemRemoved() { return mToRemove.size() > 0; }
private:


	/// The position of this hardpoint.
	Gdiplus::PointF mPosition;
	

	/// Pointer the the parent image of this hardpoint.
	CBugImage * mParent;

	/// Contains the children images of this hardpoint.
	std::vector<std::shared_ptr<CBugImage> > mChildren;

	/// Contains items to be removed from children at next update.
	std::vector<std::shared_ptr<CBugImage> > mToRemove;

	/// Contains items to be added to children at next update.
	std::vector<std::shared_ptr<CBugImage> > mToAdd;


};

