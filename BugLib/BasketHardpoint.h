/**
 * \file BasketHardpoint.h
 *
 * \author Samantha Oldenburg
 *
 * Hardpoint class used by basket to capture heads.
 */

#pragma once
#include "Hardpoint.h"
class CHead;

/**
 * Hardpoint class used by basket to capture heads.
 */
class CBasketHardpoint :
	public CHardpoint
{
public:
	CBasketHardpoint(CBugImage * parent);
	virtual ~CBasketHardpoint();

	void AddHead(std::shared_ptr<CHead> head);

	virtual void Draw(Gdiplus::Graphics * graphics);

	virtual void Spawn();
	
	void Clear();
private:

	/// Container of head objects. Used to interact with the heads specifically.
	std::vector<std::shared_ptr<CHead> > mHeads;
};

