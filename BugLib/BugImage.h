/**
 * \file BugImage.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes drawable image.
 *
 * BugImages can have children BugImages indirectly through 
 * Hardpoints, which keep track of the children's location.
 */

#pragma once
#include "stdafx.h"
#include <vector>
#include <memory>
class CHardpoint;
class CBodyPartVisitor;

/**
 * Class that describes drawable image.
 */
class CBugImage
{
public:

	CBugImage(const CBugImage &bugImage);
	
	CBugImage();
	
	virtual ~CBugImage();

	void SetCenterPoint(Gdiplus::PointF point);

	void SetParentPoint(CHardpoint * hardPoint);

	/** Gets the axis of rotation of this image.
	* \returns This image's point of locality and rotation. */
	Gdiplus::PointF GetCenterPoint() { return mCenterPoint; }
	
	void AddHardpoint(std::shared_ptr<CHardpoint>);
	
	/** Gets the scale this image will be drawn at.
	* \returns This image's scale. */
	double GetScale() { return mScale; }
	
	virtual void Update();
	
	/** Gets the center point of this image.
	* \returns This image's center point. */
	virtual Gdiplus::PointF GetCenter() { return Gdiplus::PointF(0, 0); }
	
	virtual void DrawImage(Gdiplus::Graphics * graphics);
	
	virtual void SetImage(const wchar_t *filename);
	
	virtual void Rotate(double rotation);
	
	/** Sets the rotation of this image 
	 * \param rotation The new rotation of this image in radians. */
	virtual void SetRotation(double rotation) { mRotation = rotation; }
 
	virtual void SetFrame(int frame);

	/** Gets the hardpoint that is the parent of this image.
	 * \returns The pointer to the parent hardpoint. */
	CHardpoint * GetParentPoint() { return mParentPoint; }
	
	/** Get the width of this image 
	 * \returns The width of this image in pixels */
	int GetImageWidth() { return mImage->GetWidth(); }
	
	/** Get the height of this image
	* \returns The height of this image in pixels */
	int GetImageHeight() { return mImage->GetHeight(); }
	
	/** Gets the rotation of this image
	* \returns The rotation of this image in radians.*/
	double GetRotation() { return mRotation; }
	
	/** Set the scale of this image 
	 *\param scale The new scale of this image */
	void SetScale(double scale) { mScale = scale; }
	
	virtual void AcceptBodyPartVisitor(CBodyPartVisitor * visitor);


	Gdiplus::PointF GetImageCenter();

protected:
	void UpdateChildren(Gdiplus::PointF translate);
	
	/** Get an iterator that points to the start of the 
	 * harpoint container 
	 * \returns The begin() iterator of the hardpoint container. */
	std::vector<std::shared_ptr<CHardpoint> >::iterator Begin() { return mHardpoints.begin(); }
	
	/** Get an iterator that points to the end of the
	* harpoint container
	* \returns The end() iterator of the hardpoint container. */
	std::vector<std::shared_ptr<CHardpoint> >::iterator End() { return mHardpoints.end(); }

	/** Sets whether the image should be mirrored.
	 * \param mirror True if the object should be mirrored. */
	void SetMirror(bool mirror) { mMirror = mirror; }
private:

	/// The location of the center-left of the image. Used as the center point 
	/// for rotation.
	Gdiplus::PointF mCenterPoint;
	
	/// Pointer to the hardpoint that directly owns this image.
	CHardpoint * mParentPoint = nullptr;

	/// If true, the image will be drawn flipped around the y-axis.
	bool mMirror = false;
	
	/// Container of the hardpoints of this image. Used to indirectly 
	/// interact with this image's children.
	std::vector<std::shared_ptr<CHardpoint> > mHardpoints;
	
	/// The image that will be drawn to represent this obejct.
	std::unique_ptr<Gdiplus::Bitmap> mImage = nullptr;

	/// Filename of the image
	std::wstring mFilename;

	/// Scale of the bug currently. 1.0 means actual size/
	double mScale = 1.0;

	/// Current angle of rotation of the bug in radians.
	double mRotation = 0.0;
};

