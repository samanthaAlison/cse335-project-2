/**
 * \file BodyPart.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"

#include "BodyPart.h"
#include "BugActual.h"
#include "Hardpoint.h"
#include "BodyPartVisitor.h"
#include <cstdio>
#include <cstdlib>


/**
 * Copy constructor.
 * \param bodyPart BodyPart we are copying.
 */
CBodyPart::CBodyPart(const CBodyPart & bodyPart) : CBugImage(bodyPart)
{
	mBug = bodyPart.mBug;
	mEndGrow = bodyPart.mEndGrow;
	mStartGrow = bodyPart.mStartGrow;

}


/**
 * Constructor.
 * \param bug Pointer to the bug that indirectly owns this body part
 * \param startGrow Frame to start growing this body part at.
 */
CBodyPart::CBodyPart(CBugActual * bug, int startGrow) : mBug(bug), mStartGrow(startGrow)
{
	mEndGrow = mStartGrow + mBug->GetRandom()->Random(15, 30);
}


/**
 * Destructor.
 */
CBodyPart::~CBodyPart()
{

}

/**
 * Sets the bug of this body part
 * \param bug New bug that indirectly owns this part.
 */
void CBodyPart::SetBug(CBugActual *bug)
{
	mBug = bug;
}


/**
 * Generates a filename randomly from a format and number of
 * options, and prints it to a string.
 * \param filename String to print the filename to.
 * \param format The format of the image file
 * \param numImages The number of different images to choose from.
 */
void CBodyPart::GetImageFileName(wchar_t * filename, const wchar_t * format, int numImages)
{
	CPseudoRandom * random = mBug->GetRandom();

	int imageIndex = random->Random(1, numImages);
	wsprintf(filename, format, imageIndex);

}


/**
 * Called to set the image of a body part.
 * \param format The format of the image file
 * \param numImages The number of different images to choose from.
 */
void CBodyPart::GetImageFile(const wchar_t * format, int numImages)
{
	wchar_t filename[32];
	
	GetImageFileName(filename, format, numImages);
	SetImage(filename);

}


/**
 * Gets the center point of the part. 
 * \returns The center-left point of the part's image.
 */
Gdiplus::PointF CBodyPart::GetCenter()
{
	return Gdiplus::PointF(0.0, (float)(GetImageHeight() / 2));
}


/**
 * Called when the frame of the bug changes. Ensures that rotation
 * \param frame Frame the bug is currently at.
 */
void CBodyPart::SetFrame(int frame)
{
	if (frame == mStartGrow)
	{
		if (GetRotation() - 0.0 < 0.000001)
		{
			
		}
	}
	CBugImage::SetFrame(frame);
}


/**
 * Accepts a body part visitor.
 * \param visitor Visitor to accept.
 */
void CBodyPart::AcceptBodyPartVisitor(CBodyPartVisitor * visitor)
{
	CBugImage::AcceptBodyPartVisitor(visitor);
}


/**
 * Removes this bodypart from the hardpoint it is owned by.
 */
void CBodyPart::Delete()
{
	SetParentPoint(nullptr);
}


/**
 * Updates scale of the body part based on the current frame
 */
void CBodyPart::UpdateScale()
{
	if (mBug != nullptr)
	{
		int frame = mBug->GetFrame();

		double scale = (double(frame - mStartGrow)) / (double(mEndGrow - mStartGrow));

		if (scale < 0.0)
		{
			SetScale(0.0);
		}
		else if (scale > 1.0)
		{
			SetScale(1.0);
		}
		else
		{
			SetScale(scale);
		}
	}
}


/**
 * Updates this body part. Calls the function to 
 * update the scale.
 */
void CBodyPart::Update()
{
	UpdateScale();
	CBugImage::Update();
}


/**
 * Sets the image of this body part.
 * \param filename Filename of the image to use for this body part.
 */
void CBodyPart::SetImage(const wchar_t * filename)
{
	CBugImage::SetImage(filename);
	SetCenterPoint(GetCenter());

}
