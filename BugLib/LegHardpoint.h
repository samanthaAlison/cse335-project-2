/**
 * \file LegHardpoint.h
 *
 * \author Samantha Oldenburg
 * 
 * Hardpoint class that spawns two legs.
 */

#pragma once
#include "BugHardpoint.h"
class CSegment;

/**
 * Describes a hardpoint that spawns two legs.
 */
class CLegHardpoint :
	public CBugHardpoint
{
public:
	CLegHardpoint(CSegment * part,  wchar_t * const * filename);
	virtual ~CLegHardpoint();

	virtual void Spawn();


private:
	/// Name of the image file to use for the legs.
	 wchar_t * const * mFilename;
};

