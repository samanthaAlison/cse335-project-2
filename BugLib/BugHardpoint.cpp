/**
 * \file BugHardpoint.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "BugHardpoint.h"
#include "BugActual.h"
#include "BodyPart.h"
#include "PseudoRandom.h"

/**
 * Constructor.
 * \param part The bodypart that owns this hardpoint.
 */
CBugHardpoint::CBugHardpoint(CBodyPart * part) : CHardpoint(part)
{
	mBug = part->GetBug();
	mParentStart = part->GetStartGrow();
	mParentEnd = part->GetEndGrow();
}


/**
 * Destructor.
 */
CBugHardpoint::~CBugHardpoint()
{
}

/**
 * Get the random generator this bug uses.
 * \returns Pointer to the bugs CPseudoRandom object.
 */
CPseudoRandom * CBugHardpoint::GetRandom()
{
	return mBug->GetRandom();
}


/**
 * Generate the frame at which a child of this will 
 * start to grow.
 * \returns A frame the child will start to grow at. 
 */
int CBugHardpoint::GetStartGrow()
{
	return GetRandom()->Random(
		GetParentStart() + 15,
		GetParentEnd() + 15);
}
