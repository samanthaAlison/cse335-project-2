/**
 * \file MutatedSegment.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describe a mutated segment.
 *
 * Mutated segments can grow more than one segment off of them.
 * Implements the changeling design pattern (described in
 * MutationVisitor.h).
 */

#pragma once
#include "Segment.h"
#include "SegmentHardPoint.h"
/**
 * Class that describe a mutated segment.
 */
class CMutatedSegment :
	public CSegment
{
public:
	CMutatedSegment(const CSegment & segment);
	virtual ~CMutatedSegment();

	virtual void Mutate();
};

