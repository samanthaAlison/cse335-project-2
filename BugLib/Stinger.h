/**
 * \file Stinger.h
 *
 * \author Samantha Oldenburg
 *
 * Class that defines a stinger of a bug.
 */

#pragma once
#include "BodyPart.h"

class CBugActual;
/**
 * Class that defines a stinger of a bug.
 */
class CStinger :
	public CBodyPart
{
public:
	CStinger(CBugActual * bug,  int startGrow);
	virtual ~CStinger();
};

