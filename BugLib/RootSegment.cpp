/**
 * \file RootSegment.cpp
 *
 * \author Samantha Oldenburg
 */

#include "stdafx.h"
#include "RootSegment.h"
#include "SegmentHardPoint.h"
#include "BugActual.h"
#include "BendVisitor.h"
#include "PseudoRandom.h"

/**
 * Copy constructor.
 * \param root Root segment to be copied.
 */
CRootSegment::CRootSegment(const CRootSegment & root) : CSegment(root)
{

}


/**
 * Constructor.
 * \param bug BugActual object that owns this.
 * \param startGrow Frame to start growing this body part at.
 */
CRootSegment::CRootSegment(CBugActual * bug, int startGrow) : CSegment(bug, false, startGrow)
{
	SetParentPoint(nullptr);
	std::shared_ptr<CSegmentHardpoint> leftHardPoint = std::make_shared<CSegmentHardpoint>(this, true);
	leftHardPoint->SetPosition(GetCenterPoint());
	AddHardpoint(leftHardPoint);

}


/**
 * Destructor.
 */
CRootSegment::~CRootSegment()
{
}



/**
 * Updates the hardpoints that this root owns.
 */
void CRootSegment::Update()
{
	UpdateScale();
	UpdateChildren(Gdiplus::PointF(0, 0));

}


/**
 * Bends all other segments, heads, and stingers.
 * \param bend Angle to bend the segments at.
 */
void CRootSegment::SetBend(double bend)
{

	// Get the change in bend
	double deltaBend = bend - mBend;
	mBend = bend;
	CBendVisitor visitor(deltaBend);

	CBugImage::AcceptBodyPartVisitor(&visitor);

}

/**
 * Accepts a body part visitor, and makes the roots children
 * accept it.
 * \param visitor The visitor visiting the bug.
 */
void CRootSegment::AcceptBodyPartVisitor(CBodyPartVisitor * visitor)
{
	CBugImage::AcceptBodyPartVisitor(visitor);
}


