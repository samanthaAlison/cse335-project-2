/**
 * \file MutationVisitor.h
 *
 * \author Samantha Oldenburg
 *
 * Visitor class that mutates body parts.
 *
 * Implements a changeling desgin pattern, where
 * an object in a composite is used to copy construct 
 * an object of a derived class, which then replaces the  
 * original object's position in the composite. 
 */

#pragma once
#include <string>
#include "BodyPartVisitor.h"
#include "MutatedSegment.h"
#include "PseudoRandom.h"

/**
 * Visitor class that mutates body parts.
 */
class CMutationVisitor : public CBodyPartVisitor
{
public:
	CMutationVisitor(CPseudoRandom * random, double probability, int allowed);
	~CMutationVisitor();



	virtual void VisitBodyPart(CBodyPart * bodyPart);
	virtual void VisitLeg(CLeg * leg);
	virtual void VisitSegment(CSegment * segment);
	virtual void VisitHead(CHeadActual * head);
	virtual void VisitStinger(CStinger * stinger);

private:

	/// Probability of mutations happening.
	double mProbability = 0.25;
	
	/// Mutations allowed to occur.
	int mOccurencesAllowed = 5;


	/// Pointer to random number generator
	CPseudoRandom * mRandom;
};

