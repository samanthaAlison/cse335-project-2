/**
 * \file HeadActual.h
 *
 * \author Samantha Oldenburg
 *
 * Class that describes the head of a bug actual.
 *
 * HeadActuals can be harvested into a basket.
 */

#pragma once
#include "Head.h"
#include "BodyPart.h"
class CBugActual;

/**
 * Class that describes the head of a bug actual.
 */
class CHeadActual :
	public CHead, public CBodyPart
{
public:
	/** Copy constructor.
	 *\param head Head to be copied.*/
	CHeadActual(const CHeadActual & head) : CBodyPart(head) { }
	CHeadActual(CBugActual * bug, bool left, int startGrow);



	virtual ~CHeadActual();

	virtual void SetBasketPosition(int x, int y);


	virtual void DrawInBasket(Gdiplus::Graphics *graphics);

	virtual void AcceptBodyPartVisitor(CBodyPartVisitor * visitor) override;

	/** Sets the weak pointer of this head. 
	 * \param self Pointer to this head. */
	void SetSelf(std::shared_ptr<CHeadActual> self) { mSelf = self; }
	
	/** Get a shared pointer to this head. 
	 * \returns A shared pointer to this head */
	std::shared_ptr<CHeadActual> GetSelf() { return mSelf.lock(); }

private:

	/// Weak pointer to the object itself. Used to get a shared_ptr of the object.
	std::weak_ptr<CHeadActual> mSelf;
};

